;; ------------ org-hugo-utils.el ------------ ;;

;;;* Created:   2019-04-14, 19:30    @toshiba
;; Doc:         Support for blogging with hugo and `ox-hugo'.
;;              CAVEAT: it's assumed that org files are under /content-org/ dir
;;              to change default path for org files in hugo project
;;              set `ohu--default-org-content-dir' variable.


;;;* CODE STARTS HERE:
(defvar ohu--default-org-content-dir "content-org"
  "Default directory NAME for org content in hugo project:
/path/to/hugo/project/NAME")


;;** get root dir
(defun ohu--get-root-dir (path)
  (car (split-string path ohu--default-org-content-dir t)))


;;** get relative path
(defun ohu--get-relative-path (path)
  (cadr (split-string path ohu--default-org-content-dir t)))


;;** read dir locals
(defun ohu--read-dir-locals (&optional path)
  (let ((root (ohu--get-root-dir (or path (buffer-file-name)))))
    (assoc-default 'org-mode
                   (assoc-default ohu--default-org-content-dir
                                  (dir-locals-get-class-variables
                                   (dir-locals-read-from-dir root))))))


;;** config parser
(defun ohu--hugo-config-parser (path key &optional param)
  "Parse config file from located at PATH and return value for provided key.
Assume being in ../content-org/ dir"
  (let ((conf (expand-file-name "config.toml" (ohu--get-root-dir path)))
        li)
    (if (file-exists-p conf)
        (with-temp-buffer
          (insert-file-contents conf)
          (when param
            (let ((param (if (and (string-match "\\`\\[.+\\]\\'" param)
                                (equal (match-string 0 param) param))
                             param (concat "[" param "]")))
                  beg)
              (search-forward param nil t)
              (setq beg (point))
              (re-search-forward "^[ \t]*$" nil t)
              (narrow-to-region beg (point))))
          (flush-lines "\\[\\|^ *#\\|^$")
          (setq li (mapcar
                    (lambda (x) (split-string
                            (replace-regexp-in-string "^ *" "" x) " = " t))
                    (split-string (buffer-string) "\n" t)))
          (let ((found (assoc-default key li)))
            (if found
                (replace-regexp-in-string "\"" "" (car found))
              (message (concat "hugo-config-parser: Value for key \"%s\""
                               "not found in config file: %s")
                       key conf)
              nil)))
      (user-error "Seems that config file is missing!"))))


;;** keyword parser
;; FIX: maybe use plist instead of assoc list
(defun ohu--parse-org-keyword (keyword-re)
  (org-element-map (org-element-parse-buffer 'element) 'keyword
    (lambda (el) (when (string-match keyword-re (org-element-property :key el))
              (cons (org-element-property :key el)
                    (list (list (cons 'value (org-element-property :value el))
                                (cons 'begin (org-element-property :begin el))
                                (cons 'end (org-element-property :end el)))))))))


;;** keyword reader
(defun ohu--read-org-keyword (keyword &optional key)
  (let ((li (cadr (assoc (upcase keyword) (ohu--parse-org-keyword keyword)))))
    (if key (assoc-default key li) li )))

;;** convert input
(defun ohu--convert-input-to-meta-line (type input &optional no-hugo)
  (if (string= "" input)
      ""
    (let* ((prefix (if no-hugo "" "HUGO_"))
           (input (replace-regexp-in-string
                   "(\\|)" ""
                   (format "%s" (split-string input " " t)))))
      (format "#+%s%s: %s\n" prefix type input))))


;;** completing read
(defun ohu--completing-read (type &optional path)
  (let ((str (substring (symbol-name type) 5)))
    (completing-read (format  "%s: " str)
                     (assoc-default type
                                    (ohu--read-dir-locals (or path))))))


;;** find md file
(defun ohu--find-md-file ()
  (save-restriction
    (widen)
    (let* ((path (buffer-file-name))
           (export-file (ohu--read-org-keyword "EXPORT_FILE_NAME" 'value))
           (section (concat
                     "content"
                     (replace-regexp-in-string
                      "\\." "" (ohu--read-org-keyword "HUGO_SECTION" 'value))))
           (org-file (file-name-nondirectory (file-name-sans-extension path)))
           (root-dir (ohu--get-root-dir path)))
      (expand-file-name
       (concat (if export-file export-file org-file) ".md")
       (concat root-dir section)))))


;;** ask for param
(defun ohu--ask-for-param (param locals)
  (not (member param (assoc-default 'hugo-preamble-exclude locals))))


;;** insert preamble
(defun ohu-insert-preamble ()
  "Insert an export template for org files in content-org dir in a hugo project"
  (if (y-or-n-p "Do you want to insert org hugo preable?")
      (let* ((path (buffer-file-name))
             (locals (ohu--read-dir-locals path))
             (rel-path (ohu--get-relative-path (file-name-directory path)))
             (file-no-ext (file-name-sans-extension (file-name-nondirectory path)))
             (basedir "")
             (basediratom "../")
             (author (ohu--convert-input-to-meta-line
                      "author"
                      (or (ohu--hugo-config-parser path "author")
                         (ohu--hugo-config-parser path "name" "author"))
                      t))
             (title-in (read-from-minibuffer "title: "))
             (title (ohu--convert-input-to-meta-line "title" title-in t))
             (link-title (when (ohu--ask-for-param :link-title locals)
                           (read-from-minibuffer "linkTitle: " title-in nil nil nil title-in)))
             (weight (if (ohu--ask-for-param :weight locals)
                         (ohu--convert-input-to-meta-line
                          "WEIGHT" (read-from-minibuffer "weight: "))
                       ""))
             (date (ohu--convert-input-to-meta-line "date" (format-time-string "%Y-%m-%d") t))
             (tags (if (ohu--ask-for-param :tags locals)
                       (ohu--convert-input-to-meta-line "TAGS" (ohu--completing-read 'hugo-tags))
                     ""))
             (categories_read (if (ohu--ask-for-param :categories locals)
                                  (ohu--completing-read 'hugo-categories)
                                ""))
             (categories (ohu--convert-input-to-meta-line
                          "CATEGORIES" categories_read))
             ;; this should be dealt with in dir-locals
             ;; (docs (if (string-match "book" (ohu--hugo-config-parser path "theme"))
             ;;           ":type docs " ""))
             (custom (ohu--convert-input-to-meta-line
                      "CUSTOM_FRONT_MATTER"
                      (concat
                       ;; docs
                       (unless (or (not link-title) (string= title-in link-title))
                         (format ":linkTitle \"%s\" " link-title))
                       (assoc-default 'hugo-custom locals))))
             (section (ohu--convert-input-to-meta-line
                       "SECTION"
                       (concat
                        "./"
                        (let ((_choice (if (ohu--ask-for-param :sections locals)
                                           (assoc-default 'hugo-sections locals)
                                         "")))
                          (cond ((and (listp _choice) (> (length _choice) 0))
                                 (completing-read "section: " _choice))
                                ((stringp _choice) _choice)
                                ;; FIXME: change it to read dir path from project root
                                (t (concat
                                    (substring rel-path 1)
                                    (read-from-minibuffer "section: ")))))))))
        (dotimes (x (length (replace-regexp-in-string "[^/]" "" rel-path)))
          (setq basedir (concat basedir basediratom)))
        (setq basedir (ohu--convert-input-to-meta-line "BASE_DIR" basedir))
        (insert
         (format
          (concat ":hugo-export:\n%s%s%s%s%s%s#+HUGO_DRAFT: false\n"
                  "#+OPTIONS: todo:nil e:nil d:nil\n#+TODO: write edit | done\n:end:\n"
                  "%s%s%s# update:\n\n\* ")
          section basedir custom weight tags categories title author date))
        (message "File is ready for export to markdown by hugo!"))
    (message "Hugo to org autoinsertion aborted")))


;;;* COMMANDS
;;** edit menu (I)
(defun ohu-org-hugo-book-edit-menu ()
  (interactive)
  (let* ((this (buffer-file-name))
         (root (ohu--get-root-dir this))
         (rel-path (substring (ohu--get-relative-path this) 1))
         (fileroot (file-name-sans-extension
                    (file-name-nondirectory this)))
         (where (or
                 (replace-regexp-in-string
                  "/" "" (ohu--hugo-config-parser this "BookMenuBundle"))
                 "menu"))
         (menu (concat root (format "content/%s/index.md" where)))
         (section (concat
                   (file-name-directory rel-path)
                   (if (string= fileroot "_index") "" fileroot)))
         (menuitem (format "- {{< menuitem \"%s\" >}}" section)))
    (if (file-exists-p menu)
        (progn (kill-new menuitem)
               (find-file menu)
               (message "Position %s ready to insert" section))
      (user-error
       "It seems that you are in a bad location or menu file does not exit"))))

;;** delete files
(defun ohu-delete-files ()
  "Delete both org and corresponding md file"
  (interactive)
  (if (y-or-n-p "Do you want to delete this ORG and corresponding MD file? ")
      (let ((md-file (ohu--find-md-file)))
        (dolist (file (list md-file (buffer-file-name)))
          (when (file-exists-p file)
            (delete-file file t)
            (message "Org Hugo Utils: deleted %s" file))))))

;;** toggle draft
;; FIX: use org element api to put property at begininng
;; or use `org-macro--find-keyword-value'
(defun ohu-toggle-draft ()
  (interactive)
  (let* ((key "hugo_draft")
         (beg (ohu--read-org-keyword key 'begin))
         (value (ohu--read-org-keyword key 'value))
         (toggled (if (equal value "false") "true" "false"))
         bounds)
    (save-excursion
      (if beg
          (when
              (progn
                (goto-char beg)
                (search-forward ": " (line-end-position))
                (when (setq bounds (bounds-of-thing-at-point 'word))
                  (delete-region (car bounds) (cdr bounds))
                  (insert (format "%s" toggled))
                  t))
            (message "%s set to %s" (upcase key) toggled))
        (let ((base_key "hugo_base_dir")
              (base (ohu--read-org-keyword base_key 'end)))
          (if base
              (progn
                (goto-char end)
                (insert "#+HUGO_DRAFT: true\n"))
            (message "no %s key in this buffer" (upcase base_key))))))))

;;** export dwim
(defun ohu-ox-hugo-export-dwim ()
  (interactive)
  (save-restriction
    (when (buffer-narrowed-p) (widen))
    (org-hugo-export-to-md)))

;;** open md file
(defun ohu-open-md-file (&optional p)
  "Assuming point is in org file which is exported to markdown file in hugo project, open corresponding .md file"
  (interactive "P")
  (let ((md-file (ohu--find-md-file)))
    (if (file-exists-p md-file)
        (if p (find-file-other-window md-file) (find-file md-file))
      (message "It seems that markdown file '%s' hasn't been exported yet" md-file))))


;;;* DEFINE MINOR MODE
;;;###autoload
(define-minor-mode ohu-mode
  "Ox hugo enhancement."
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "C-c p") #'projectile-ibuffer)
            (define-key map (kbd "C-c m") #'ohu-org-hugo-book-edit-menu)
            (define-key map (kbd "C-c o") #'ohu-open-md-file)
            (define-key map (kbd "C-c D") #'ohu-delete-files)
            (define-key map (kbd "C-c E") #'ohu-ox-hugo-export-dwim)
            map))


;;;* HOOKS
;;** insert preamle
(defun ohu--insert-preamble ()
  (with-current-buffer (buffer-name)
    (unless (file-exists-p (expand-file-name (file-name-nondirectory (buffer-file-name))))
      (ohu-insert-preamble))))


;;** export
(defun ohu--export ()
  (save-restriction
    (when (buffer-narrowed-p) (widen))
    (when (ohu--read-org-keyword "hugo_section" 'value)
      (org-hugo-export-to-md))))


;;** add hooks
;;;###autoload
(add-hook 'ohu-mode-hook
          (lambda ()
            (if ohu-mode
                (add-hook 'after-save-hook
                          #'ohu--export nil t)
              (remove-hook 'after-save-hook
                           #'ohu--export))))

;;;###autoload
(add-hook 'ohu-mode-hook #'ohu--insert-preamble)


;;;* PROVIDE
(provide 'org-hugo-utils)


;; org-hugo-utils.el ends here
