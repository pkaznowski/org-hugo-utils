;;; -*- lexical-binding: t -*-

;; ------------ org-hugo-utils-test.el ------------ ;;
;; Created:     2019-08-09, 13:28    @x200
;; Last update: 2019-08-09, 17:50:39 @x200

;; Code starts here:
(require 'org-hugo-utils)
;; (require 'ert)
;; (require 'el-mock)
(require 'buttercup)

;; (ert-deftest get-root-dir-test ()
;;   (let ((path "/home/user/page/content-org/blog/file.org"))
;;     (with-mock
;;       (stub ohu--default-org-content-dir => "content-org")
;;       (should (string= (ohu--get-root-dir path) "/home/user/page/")))))

;; (ert-deftest get-relative-path-test ()
;;   (let ((path "/home/user/page/content-org/blog/file.org"))
;;     (with-mock
;;       (stub ohu--default-org-content-dir => "content-org")
;;       (should (string= (ohu--get-relative-path path) "/blog/file.org")))))

(describe "Path funcs suite"
  :var (ohu--default-org-content-dir
        path)

  (before-each
    (setq ohu--default-org-content-dir "content-org")
    (setq path "/home/user/page/content-org/blog/file.org"))

  (it "test ohu--get-root-dir"
    (expect (ohu--get-root-dir path)
            :to-equal
            "/home/user/page/"))

  (it "test ohu--get-relative-path"
    (expect (ohu--get-relative-path path)
            :to-equal
            "/blog/file.org")))

;; org-hugo-utils-test.el ends here
